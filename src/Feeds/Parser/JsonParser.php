<?php

/**
 * @file
 * Contains \Drupal\feeds_json_parser\Feeds\Parser\JsonParser.
 */

namespace Drupal\feeds_json_parser\Feeds\Parser;

use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Item\DynamicItem;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Plugin\Type\Parser\ParserInterface;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\StateInterface;

/**
 * Defines a JSON feed parser.
 *
 * @FeedsParser(
 *   id = "json",
 *   title = @Translation("JSON"),
 *   description = @Translation("Parse JSON sources.")
 * )
 */
class JsonParser extends PluginBase implements ParserInterface {

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state) {
    $result = new ParserResult();

    // decode json 
    $raw = $fetcher_result->getRaw();
    $data = json_decode($raw);
    
    // @todo parsing
    
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingSources() {
    return FALSE;
  }
}
